
import time
import ubinascii
import machine
import network
import ussl

import leds
import bl00mbox

from umqtt.simple import MQTTClient

from st3m.application import Application, ApplicationContext
from st3m.input import InputState
from ctx import Context
import st3m.run

blm = bl00mbox.Channel()
# set channel volume
blm.volume = 5000      

def load_file(path) -> bytes: 
    f = open(path)
    data = f.read()
    f.close()
    return bytes(data, 'utf-8')

def wifi_low_err_code(code) -> str:
    if code == 201:
        return "WIFI_REASON_NO_AP_FOUND"
    return code

def wheel(pos):
  # Input a value 0 to 255 to get a color value.
  # The colours are a transition r - g - b - back to r.
  if pos < 0 or pos > 255:
    return (0, 0, 0)
  if pos < 85:
    return (255 - pos * 3, pos * 3, 0)
  if pos < 170:
    pos -= 85
    return (0, 255 - pos * 3, pos * 3)
  pos -= 170
  return (pos * 3, 0, 255 - pos * 3)

def rainbow_cycle(wait, tiny):
    num_leds = 40
    for j in range(0, 255, 5):
        for i in range(num_leds):
            rc_index = (i * 256 // num_leds) + j
            rgb = wheel(rc_index & 255)
            leds.set_rgb(i, rgb[0], rgb[1], rgb[2])
            tiny.signals.pitch.tone = i
            tiny.signals.trigger.start()
            tiny.signals.trigger.stop()
        leds.update()
        time.sleep_ms(wait)
        tiny.signals.trigger.stop()

class LightningTap(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)

        try:
            self.bundle_path = app_ctx.bundle_pathh
        except:
            self.bundle_path = "/flash/sys/apps/lightning_tap"

        self.SSID = "Camp2023-open"

        self.TOPIC = b"e3ed386d6cbf0c2431f9fb76/lightning-tap/0"
        self.BROKER_ADDRESS = b"544ddf6b5ad74e2b985d27619aed8051.s1.eu.hivemq.cloud"
        self.BROKER_USER = b'lightning_tap_app'
        self.BROKER_PASS = b'Lightning_tap_app_0'
        self.BROKER_PORT = 8883
        self.BROKER_CA = load_file("/flash/sys/apps/lightning_tap/mqtt_server.pem")
        self.QOS = 0 # QoS: At Most Once
        self.client_id = ubinascii.hexlify(machine.unique_id())

        self.BOTTLE_PATH = f"{self.bundle_path}/beer_bottle.png"
        self.BEER_TAPPED = 0

        self.wlan_sta = network.WLAN(network.STA_IF)
        # network can be setup via settings since 1.2.0
        if not self.wlan_sta.isconnected():
            print("wifi not connected setting it up")
            self.init_wifi()

        self.mqtt_client = MQTTClient(
            self.client_id, 
            self.BROKER_ADDRESS, 
            port=self.BROKER_PORT, 
            user=self.BROKER_USER, 
            password=self.BROKER_PASS,
            ssl=True,
            ssl_params = {
                'cadata': self.BROKER_CA, 
                'cert_reqs': ussl.CERT_REQUIRED,
                'server_hostname': '544ddf6b5ad74e2b985d27619aed8051.s1.eu.hivemq.cloud' },
            keepalive=60
            )
        self.mqtt_connected = False

        self.wait_for_draw = True

        self.show_error = None
        self.show_error_reason = None
        
        self.tiny = blm.new(bl00mbox.patches.tinysynth_fm)
        self.tiny.signals.output = blm.mixer
        self.tiny.signals.trigger.start()
        self.tiny.signals.waveform = 0
        self.tiny.signals.trigger.stop()

    def init_wifi(self):
        if self.wlan_sta.active():
            self.wlan_sta.disconnect()
            self.wlan_sta.active(False)
            time.sleep_ms(100) 
        self.wlan_sta.config(reconnects=10)
        self.wlan_sta.active(True)

    def connect_wifi(self, init = False) -> None:
        try:
            print('scanning wifi networks')
            print(self.wlan_sta.scan())
            print('connecting to %s' % self.SSID)
            self.wlan_sta.connect(self.SSID)
            while not self.wlan_sta.isconnected():
                status = self.wlan_sta.status()
                if status < 1000:
                    print("wifi disconnected: %s" % wifi_low_err_code(status))
                    self.show_error = "wifi_error" 
                    self.show_error_reason = wifi_low_err_code(status)
                    self.init_wifi()
                    return
                pass
            self.show_error = None
            print('connected to %s' % self.SSID)
        except OSError as e:
            if str(e) == "Wifi Internal Error":
                print("Wifi Internal Error, resetting wifi")
                self.init_wifi()
                return
            print("exception", str(e))
        
    def sub_cb(self, topic, msg) -> None:
        print((topic, msg))
        if topic == b'notification' and msg == b'received':
            print('ESP received hello message')
        self.BEER_TAPPED = 3

    def connect_and_subscribe(self) -> None:
        try:
            self.mqtt_client.connect()
            self.mqtt_client.set_callback(self.sub_cb)
            self.mqtt_client.subscribe(self.TOPIC)
            self.mqtt_connected = True
        except OSError as e:
            print("exception", str(e))
        except IndexError as e:
            print("exception", str(e))

    def draw(self, ctx: Context) -> None:
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()

        ctx.move_to(0, -90)
        ctx.text_align = ctx.CENTER
        ctx.text_baseline = ctx.MIDDLE
        ctx.font_size = 20
        if self.mqtt_connected:
            ctx.rgb(0, 255, 255).text("connected")
        else:
            ctx.rgb(0, 255, 255).text("connecting ...")

        if self.show_error:
            ctx.move_to(0, 30)
            ctx.font_size = 15
            ctx.rgb(0, 255, 255).text(self.show_error)
            if self.show_error_reason:
                ctx.move_to(0, 45)
                ctx.rgb(0, 255, 255).text(self.show_error_reason)
        
        if self.BEER_TAPPED == 3:
            leds.set_brightness(200)
            ctx.image(self.BOTTLE_PATH, -70, -60, 120, 120).fill()
            rainbow_cycle(1, self.tiny)
            self.BEER_TAPPED -= 1
        elif self.BEER_TAPPED == 2:
            ctx.image(self.BOTTLE_PATH, -60, -60, 120, 120).fill()
            rainbow_cycle(1, self.tiny)
            self.BEER_TAPPED -= 1
        elif self.BEER_TAPPED == 1:
            ctx.image(self.BOTTLE_PATH, -70, -60, 120, 120).fill()
            rainbow_cycle(1, self.tiny)
            leds.set_brightness(0)
            leds.update()
            self.BEER_TAPPED -= 1
        else:
            ctx.image(self.BOTTLE_PATH, -60, -60, 120, 120).fill()

    def reset_mqtt(self) -> None:
        self.mqtt_connected = False
        print('Failed to connect to MQTT broker. Reconnecting...')
        time.sleep(10)

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms) # Let Application do its thing
        if self.wait_for_draw:
            self.wait_for_draw = False
            return
        if self.wlan_sta.isconnected():
            try:
                if self.mqtt_connected:
                    self.mqtt_client.check_msg()
                else:
                    self.connect_and_subscribe()
            except OSError as e:
                print("exception", str(e))
                self.mqtt_client.disconnect()
                self.mqtt_connected = False
        else:
            self.mqtt_connected = False
            self.connect_wifi()             

if __name__ == '__main__':
    st3m.run.run_view(LightningTap(ApplicationContext()))